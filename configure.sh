#!/bin/bash

################################################################################
#                                                                              #
#                                 {o,o}                                        #
#                                 |)__)                                        #
#                                 -"-"-                                        #
#                                                                              #
################################################################################

#################################---ENV---######################################

set -e
set -u

################################################################################

##############################---FUNCTIONS---###################################

function configureApache() {
    echo "INFO: Configuring apache"
    a2enmod proxy_http proxy_html rewrite headers proxy
    pear config-set php_dir /usr/lib/php/pear
    pear config-set doc_dir /usr/lib/php/pear/docs
    pear config-set cfg_dir /usr/lib/php/pear/cfg
    pear config-set data_dir /usr/lib/php/pear/data
    pear config-set test_dir /usr/lib/php/pear/tests
    pear config-set www_dir /usr/lib/php/pear/www
    pecl install raphf-stable
    pecl install propro-stable
    pecl install pecl_http
    sed -i 's|^    RPAFproxy_ips.*|    RPAFproxy_ips 172.16.0.0/12|' /etc/apache2/mods-available/rpaf.conf
}

function configurePhp() {
    echo "INFO: Configuring php"
    echo 'date.timezone = "Europe/Ljubljana"
        extension=raphf.so
        extension=propro.so
        extension=http.so' >> /etc/php/7.0/apache2/php.ini

    echo 'date.timezone = "Europe/Ljubljana"
        extension=raphf.so
        extension=propro.so
        extension=http.so' >> /etc/php/7.0/cli/php.ini


    sed -i "s/^error_reporting.*/error_reporting = E_ALL \& ~E_USER_DEPRECATED \& ~E_DEPRECATED \& ~E_STRICT/" /etc/php/7.0/apache2/php.ini; \
    sed -i "s/^memory_limit.*/memory_limit = 512M/" /etc/php/7.0/apache2/php.ini
}

################################################################################

###############################---EXECUTION---##################################

configurePhp
configureApache

################################################################################
