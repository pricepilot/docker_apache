    <?php

//Do not allow to run php scripts as root. They should be run as www-data to prevent symfony cache and log dir issues

$userRunningPhpScript = posix_getpwuid( posix_geteuid() )['name'];

if ( 'root' == $userRunningPhpScript ) {

	$tl =  PHP_EOL . PHP_EOL;

	echo $tl;
	echo '*************************** PricePilot *****************************' . $tl;
	echo "\033[31m";
	echo ' WARNING! You should run php scripts as www-data user.';
	echo "\033[0m" . $tl;
	echo ' Login to container with -u www-data or contact the administrator.' . $tl;
	echo '********************************************************************' . $tl;

	die();

}
